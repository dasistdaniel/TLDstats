let tlds = {};

function storageReset() {
  tlds = {};
  chrome.storage.local.set({ tlds });
}

function logURL(requestDetails) {
  console.log(requestDetails.url);
  chrome.storage.local.get("tlds", function (result) {
    tlds = result.tlds;
  });

  const url = requestDetails.url;
  const domaininfo = getDomain(url);
  const protocol = domaininfo[0];
  const domain = domaininfo[1];
  const tld = getTld(domain);

  if (protocol.indexOf("http") >= 0) {
    countTldPlus(tld);
  } else {
    console.log("no http(s)");
  }
}

function getDomain(url) {
  // I hate Regex
  segments = url.split("/");
  return [segments[0], segments[2]];
}

function getTld(domain) {
  // Did i say, that i hate regex?
  return domain.split(".").pop();
}

function countTldPlus(tld) {
  if (tld.length < 10) {
    if (tlds.hasOwnProperty(tld)) {
      tlds[tld]["count"] += 1;
    } else {
      tlds[tld] = { count: 1 };
    }
    chrome.storage.local.set({ tlds });
  } else {
    console.log("this is no tld:", tld);
  }
}

chrome.webRequest.onBeforeRequest.addListener(logURL, {
  urls: ["<all_urls>"],
});
