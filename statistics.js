let tlds = {};
let tldsCount = 0;

let chartLabels = [];
let chartData = [];

let chartColors = [];

let ctx = document.getElementById("myChart").getContext("2d");
let myChart;

let btnReset = document
  .getElementById("btnResetStats")
  .addEventListener("click", resetStats);

function resetStats() {
  tlds = {};
  chrome.storage.local.set({ tlds });
  myChart.data.labels = [];
  myChart.data.datasets = [];
  myChart.update();
}

function loadChart() {
  chrome.storage.local.get("tlds", function (result) {
    tlds = result.tlds;
    tldsCount = Object.keys(tlds).length;
    chartColors = generateHslColors(43, 52, tldsCount);

    for (const tld in tlds) {
      if (Object.hasOwnProperty.call(tlds, tld)) {
        chartLabels.push("." + tld + " (" + tlds[tld].count + ")");
        chartData.push(tlds[tld].count);
      }
    }

    myChart = new Chart(ctx, {
      type: "doughnut",
      data: {
        labels: chartLabels,

        datasets: [
          {
            data: chartData,
            backgroundColor: chartColors,
            borderWidth: 1,
            borderColor: "grey",
          },
        ],
      },
      options: {
        title: {
          display: true,
          text: "Statistics",
          fontSize: 22,
          fontWeight: "bold",
        },
        legend: {
          display: true,
          position: "bottom",
          labels: {
            fontColor: "black",
            fontSize: 14,
            fontWeight: "bold",
          },
        },
      },
    });
  });
}
// Found on https://mika-s.github.io/javascript/colors/hsl/2017/12/05/generating-random-colors-in-javascript.html
function generateHslColors(saturation, lightness, amount) {
  let colors = [];
  let huedelta = Math.trunc(360 / amount);

  for (let i = 0; i < amount; i++) {
    let hue = i * huedelta + 10;
    colors.push(`hsl(${hue},${saturation}%,${lightness}%)`);
  }

  return colors;
}

loadChart();
